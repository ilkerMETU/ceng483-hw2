import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import argparse
import math
import os
import pickle
import sys
from collections import Counter

import cv2
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score, confusion_matrix

ap = argparse.ArgumentParser()
ap.add_argument("--train", required=True, help="path to the train images")
ap.add_argument("--validation", required=False, help="path to the validation images")
ap.add_argument("--test", required=False, help="path to the test image")
ap.add_argument("--kmeans",required=True, help="k-means K value")
ap.add_argument("--kNN",required=True, help="kNN K value")
ap.add_argument("--step",required=True, help="Dense-SIFT step size")
args = vars(ap.parse_args())

vocab_size = int(args["kmeans"])
kNN_K = int(args["kNN"])
denseSIFT_step = int(args["step"])

dense_string = "dense_" + str(denseSIFT_step) if (denseSIFT_step != 0) else ""
conf_mat_dir = "../conf_mats/" + dense_string + "_kmeans_" + str(vocab_size) + "_kNN_" + str(kNN_K)

# Get class name of the given image path
def get_class_name(image_path):
    return image_path.split("/")[-2]

# Get beasename of the image
def get_basename(image_path):
    return image_path.split("/")[-1]

# Calculates euclidian distance betwwen given feature vectors
def euclidian_distance(v1, v2):
    dist = [(x - y)**2 for x, y in zip(v1, v2)]
    dist = math.sqrt(sum(dist))
    return dist

def plot_conf_mat(conf_mat):
    labels = ['apple', 'aquarium\nfish','beetle','camel','crab','cup',
        'elephant','flat\nfish','lion','mush-\nroom','orange','pear',
        'road','sky-\nscraper','    woman']
    fig = plt.figure(figsize=(20,20))
    ax = fig.add_subplot(111)
    cax = ax.matshow(conf_mat)  
    plt.title('Confusion matrix of the classifier')
    fig.colorbar(cax)
    ax.set_xticks(np.arange(len(labels)))
    ax.set_yticks(np.arange(len(labels)))
    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)
    plt.xlabel('Predicted')
    plt.ylabel('Truth')
    plt.savefig(conf_mat_dir + ".png")

# Returns SIFT descriptor of the given image
def get_SIFT_descriptor(image_path, step=0):
    
    image = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2GRAY)
    sift = cv2.xfeatures2d.SIFT_create(0, # n_feature
                                       3, # nOctaveLayers
                                       0.14, # constrastThreshold
                                       10, # edgeThreshold
                                       0.27) # sigma

    # Use dense SIFT
    if(step != 0):
       kp = [
        cv2.KeyPoint(x, y, step)
        for x in range(0, image.shape[0], step)
        for y in range(0, image.shape[1], step)
    ]
    # Use normal SIFT
    else: 
        kp = sift.detect(image, None)
        if kp is None or len(kp) == 0:
            kp = [
            cv2.KeyPoint(x, y, 8)
            for x in range(0, image.shape[0], 8)
            for y in range(0, image.shape[1], 8)
        ]
    
    kp, descriptor = sift.compute(image, kp)
    # Normalize
    descriptor = [dp / sum(dp) for dp in descriptor]
    # Remove illumination effects (contrast threshold)
    descriptor = np.clip(descriptor, 0, 0.2)
    # Normalize again
    descriptor = [dp / sum(dp) for dp in descriptor]
    return descriptor

# Creates BoF  vocabulary
def create_vocabulary():
    dp = []
    for dirpath, dirnames, filenames in os.walk(args["train"]):
        for filename in filenames:
            image_path = os.path.join(dirpath, filename)
            descriptor = get_SIFT_descriptor(image_path, denseSIFT_step)
            dp.extend(descriptor)

    kmeans = KMeans(n_clusters=vocab_size,
                    max_iter = 500,
                    random_state = 0).fit(dp)
    return kmeans

# Returns the prediction histogram of the image
def get_prediction_histogram(prediction):
    histogram, _ = np.histogram(prediction, bins=vocab_size, density=False)
    histogram = histogram / math.sqrt(sum(histogram ** 2))
    return histogram

# Returns all histograms of the prediction of the train images
def get_neighbors(vocabulary):
    neighbors = []
    for dirpath, _ , filenames in os.walk(args["train"]):
        for filename in filenames:
            image_path = os.path.join(dirpath, filename)
            descriptor = get_SIFT_descriptor(image_path, denseSIFT_step)
            prediction = vocabulary.predict(descriptor)
            histogram = get_prediction_histogram(prediction)
            label = get_class_name(image_path)
            neighbors.append( (label, histogram) )
    return neighbors

# Applies kNN classifier
def kNN(input_image_path, neighbors, vocabulary):
    input_descriptor = get_SIFT_descriptor(input_image_path, denseSIFT_step)
    input_prediction = vocabulary.predict(input_descriptor)
    input_histogram = get_prediction_histogram(input_prediction)

    distances = [ (label, euclidian_distance(input_histogram, hist) ) 
                    for label, hist in neighbors]
    distances = sorted(distances, key = lambda x : x[1])[0:kNN_K]

    counts = Counter(x[0] for x in distances)
    # Find max occurence count of the max occured classes
    max_occurence = max( [counts[x[0]] for x in distances] )

    # Eliminate classes where occurence is lower than max
    highest_classes = list(filter(lambda x: counts[x[0]] >= max_occurence, distances))
    # Sort by euclidian distance ascending
    category = sorted(highest_classes, key=lambda x:x[1])

    return category[0][0]
    
def main():
   
    vocabulary = create_vocabulary()
    neighbors = get_neighbors(vocabulary)

    if(args["validation"] is not None ):
        truth = []
        pred = []
        class_names = ['apple', 'aquarium_fish','beetle','camel','crab','cup',
            'elephant','flatfish','lion','mushroom','orange','pear',
            'road','skyscraper','woman']

        # Iterate all validation images
        for dirpath, _ , filenames in os.walk(args["validation"]):
            for filename in filenames:
                image_path = os.path.join(dirpath, filename)

                groundtruth = get_class_name(image_path)
                prediction = kNN(image_path, neighbors, vocabulary)
                truth.append(groundtruth)
                pred.append(prediction)

        conf_mat = confusion_matrix(truth, pred, labels=class_names)
        plot_conf_mat(conf_mat)
        acc = accuracy_score(truth, pred, normalize=True)
        print(conf_mat)
        print("Accuracy : " + str(acc))

    elif(args["test"] is not None) :
        # Iterate all test images
        
        for dirpath, _ , filenames in os.walk(args["test"]):
            for filename in filenames:
                with open("output.txt","a+") as f:
                    image_path = os.path.join(dirpath, filename)
                    prediction = kNN(image_path, neighbors, vocabulary)
                    
                    f.write("{} : {}\n".format(get_basename(image_path), prediction))

if __name__ == "__main__":
    main()
