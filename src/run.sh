#!/bin/bash

for vocab_K in 32;
do
    for step_size in 4 ;
    do
        for kNN_K in 2 4 8 30 50 100 200;
        do  
            echo 'vocab size : '$vocab_K'  , step size :'$step_size'  , kNN : '$kNN_K;
            python3 kNN.py \
            --train=../dataset/train/ \
            --validation=../dataset/validation/ \
            --kmeans=$vocab_K \
            --kNN=$kNN_K \
            --step=$step_size
        done
    done
done
